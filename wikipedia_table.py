import json
import pathlib
import urllib.request

import bs4


BASE = "https://en.wikipedia.org"


def parse_with_nominees(soup):
    table = soup.find_all("table", limit=2)[1]

    data = {}
    for tr in table.tbody.find_all("tr")[1:]:
        th = tr.find("th")

        if th:
            year = th.a.string
            data[year] = []

        results = tr.find_all("td")

        td = results[0]
        strings = td.stripped_strings
        name = next(strings)
        if name == "(no award)+":
            # no award
            continue
        author = {"name": name}
        try:
            author["link"] = BASE + td.a["href"]
        except TypeError:
            pass

        position = 2  # default: runner up
        try:
            if next(strings)[-1] == "*":
                position = 1  # winner
        except StopIteration:
            pass

        try:
            td = results[1]
            novel = {"name": next(td.stripped_strings)}
            try:
                novel["link"] = BASE + td.a["href"]
            except TypeError:
                pass
        except IndexError:
            # the tr has only a single td
            # this means the row is for an additional author on the previous entry
            data[year][-1]["authors"].append(author)
            continue

        data[year].append(
            {
                "position": position,
                "authors": [author],
                "novel": novel,
            }
        )

    return data


def parse_winners(soup):
    table = soup.find("table")

    data = {}
    for tr in table.tbody.find_all("tr")[1:]:
        year = tr.find("th").a.string

        results = tr.find_all("td")

        td = results[0]
        string = next(td.stripped_strings)
        if string == "Not awarded":
            continue
        novel = {"name": string}
        try:
            novel["link"] = BASE + td.a["href"]
        except TypeError:
            pass

        td = results[1]
        author = {"name": next(td.stripped_strings)}
        try:
            author["link"] = BASE + td.a["href"]
        except TypeError:
            pass

        data[year] = [
            {
                "position": 1,
                "authors": [author],
                "novel": novel,
            }
        ]

    return data


def scrape(name, page, with_nominees, v=False):
    """
    for tables of winners/awardees (and nominees/runner-ups)
    """
    cache = pathlib.Path(f"{name}.json")

    if cache.is_file():
        with open(cache, "r") as f:
            if v:
                print(f"Reading {cache}")
            return json.load(f)

    file = pathlib.Path(f"{name}.html")

    if not file.is_file():
        url = f"{BASE}/wiki/{page}"
        if v:
            print(f"Opening {url}")
        with urllib.request.urlopen(url) as f:
            html = f.read().decode()
        file.write_text(html)
    else:
        if v:
            print(f"Opening {file}")
        html = file.read_text()

    soup = bs4.BeautifulSoup(html, features="html.parser")

    data = (parse_with_nominees if with_nominees else parse_winners)(soup)

    with open(cache, "w") as f:
        json.dump(data, f)

    return data
