#!/usr/bin/env python3

import argparse
import collections
import datetime
import itertools
import json

import wikipedia_table as wt


def get_args():
    parser = argparse.ArgumentParser(
        description="Score novel and authors by their awards"
    )
    parser.add_argument(
        "-w",
        "--winner-score",
        type=int,
        metavar="W",
        default=3,
        help="score for winner",
    )
    parser.add_argument(
        "-r",
        "--runner-up-score",
        type=int,
        metavar="R",
        default=1,
        help="score for runner up",
    )
    parser.add_argument(
        "-y",
        "--years",
        type=int,
        metavar="Y",
        default=5,
        help="number of years to go back from now when scoring",
    )
    parser.add_argument(
        "-c",
        "--cutoff",
        type=int,
        metavar="C",
        default=2,
        help="minimum score for listing entry",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="print runtime info",
    )
    return parser.parse_args()


def same(a, b):
    if a["novel"]["name"].lower() == b["novel"]["name"].lower():
        return True

    try:
        if a["novel"]["link"] == b["novel"]["link"]:
            return True
    except KeyError:
        pass

    return False


if __name__ == "__main__":
    args = get_args()

    print("Let's see...")

    awards = [
        # 1953 -- / US / amateur
        # https://en.wikipedia.org/wiki/Hugo_Award_for_Best_Novel
        wt.scrape("hugo", "Hugo_Award_for_Best_Novel", True, args.verbose),
        # 1966 -- / US / pro
        # https://en.wikipedia.org/wiki/Nebula_Award_for_Best_Novel
        wt.scrape("nebula", "Nebula_Award_for_Best_Novel", True, args.verbose),
        # 1969 -- / UK
        # https://en.wikipedia.org/wiki/BSFA_Award_for_Best_Novel
        wt.scrape("bfsa", "BSFA_Award_for_Best_Novel", True, args.verbose),
        # 1973 -- 2019 / US
        # https://en.wikipedia.org/wiki/John_W._Campbell_Memorial_Award_for_Best_Science_Fiction_Novel
        wt.scrape(
            "jwc",
            "John_W._Campbell_Memorial_Award_for_Best_Science_Fiction_Novel",
            True,
            args.verbose,
        ),
        # 1978 -- / US
        # https://en.wikipedia.org/wiki/Locus_Award_for_Best_Fantasy_Novel
        wt.scrape(
            "locus_fantasy",
            "Locus_Award_for_Best_Fantasy_Novel",
            False,
            args.verbose,
        ),
        # 1980 -- / US
        # https://en.wikipedia.org/wiki/Locus_Award_for_Best_Science_Fiction_Novel
        wt.scrape(
            "locus_scifi",
            "Locus_Award_for_Best_Science_Fiction_Novel",
            False,
            args.verbose,
        ),
        # 1971 -- 1979 / US
        # https://en.wikipedia.org/wiki/Locus_Award_for_Best_Novel
        wt.scrape(
            "locus_old",
            "Locus_Award_for_Best_Novel",
            False,
            args.verbose,
        ),
        # 1987 -- / UK
        wt.scrape(
            "acc",
            "Arthur_C._Clarke_Award",
            True,
            args.verbose,
        ),
        # 1995 -- / AU
        # https://en.wikipedia.org/wiki/Aurealis_Award_for_Best_Fantasy_Novel
        wt.scrape(
            "aurealis_fantasy",
            "Aurealis_Award_for_Best_Fantasy_Novel",
            True,
            args.verbose,
        ),
        # 1995 -- / AU
        # https://en.wikipedia.org/wiki/Aurealis_Award_for_Best_Science_Fiction_Novel
        wt.scrape(
            "aurealis_scifi",
            "Aurealis_Award_for_Best_Science_Fiction_Novel",
            True,
            args.verbose,
        ),
    ]

    first = int(min(itertools.chain.from_iterable(x.keys() for x in awards)))
    end = datetime.date.today().year + 1
    start = end - args.years
    assert start > first

    print("\nYears")
    r = {}
    for year in range(start, end):
        r[year] = []

        for award in awards:
            try:
                for x in award[str(year)]:
                    score = (
                        args.winner_score
                        if x["position"] == 1
                        else args.runner_up_score
                    )
                    try:
                        check = next(z for z in r[year] if same(x, z))
                        check["score"] += score
                    except StopIteration:
                        r[year].append(
                            {
                                "novel": x["novel"],
                                "authors": x["authors"],
                                "score": score,
                            }
                        )
            except KeyError:
                pass

        print(year)
        for x in sorted(
            sorted(r[year], key=lambda a: min(b["name"] for b in a["authors"])),
            key=lambda a: a["score"],
            reverse=True,
        ):
            score = x["score"]
            if score >= args.cutoff:
                authors = ", ".join(z["name"] for z in x["authors"])
                print(f"{score:<2} {authors:<24} {x['novel']['name']}")

    print("\nNovels")
    names = {}
    by = {}
    novels = collections.Counter()
    for x in r.values():
        for y in x:
            name = y["novel"]["name"]
            key = name.lower()
            names[key] = name
            by[key] = [z["name"] for z in y["authors"]]
            novels[key] += y["score"]
    novels = [(n, s) for n, s in novels.items() if s >= args.cutoff]
    lim = 50
    names = {k: v if len(v) < lim else f"{v[:lim - 3]}..." for k, v in names.items()}
    nl = max(map(len, names.values()))
    sl = max(len(str(x[1])) for x in novels)
    for novel, score in sorted(
        sorted(novels, key=lambda x: x[0]), key=lambda x: x[1], reverse=True
    ):
        print(f"{score:<{sl}} {names[novel]:<{nl}} ({', '.join(by[novel])})")

    print("\nAuthors")
    authors = collections.Counter()
    books = collections.defaultdict(set)
    for x in r.values():
        for y in x:
            for a in y["authors"]:
                b = a["name"]
                authors[b] += y["score"]
                books[b].add(y["novel"]["name"].lower())
    authors = [(a, s) for a, s in authors.items() if s >= args.cutoff]
    al = max(len(x[0]) for x in authors)
    sl = max(len(str(x[1])) for x in authors)
    for author, score in sorted(
        sorted(authors, key=lambda x: x[0]), key=lambda x: x[1], reverse=True
    ):
        print(
            f"{score:<{sl}} {author:<{al}} ({', '.join(names[x] for x in books[author])})"
        )

    with open("combined.json", "w") as f:
        json.dump(r, f)
