# [speculative-awards](https://gitlab.com/eidoom/speculative-awards)

* https://en.wikipedia.org/wiki/Category:Lists_of_speculative_fiction-related_award_winners_and_nominees
* https://en.wikipedia.org/wiki/Category:Speculative_fiction_awards
* https://en.wikipedia.org/wiki/Category:Science_fiction_awards
* https://en.wikipedia.org/wiki/Category:Fantasy_awards

```shell
pipenv install
pipenv run ./combine.py --help
pipenv run ./combine.py
```
